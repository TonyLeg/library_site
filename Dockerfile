FROM node:12-alpine AS build
WORKDIR /code
ADD package* /code
RUN npm install
ADD . /code
RUN npm run build
FROM nginx:1.25.4-alpine-slim
COPY --from=build /code/build /usr/share/nginx/html
ADD nginx/app.conf /etc/nginx/conf.d/
CMD ["nginx", "-g", "daemon off;"]